import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as anime from 'animejs';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit, AfterViewInit {

  animation = null;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.animation = anime({
      targets: '.el',
      translateX: [
        { value: 250, duration: 1000, easing: 'easeInOutSine' },
        { value: 0, duration: 1000, easing: 'easeInOutSine' }
      ],
      duration: 2000,
      loop: false,
      autoplay: false
    });
  }

  animate() {
    console.log('play');
    this.animation.reset();
    this.animation.play();
  }

}
